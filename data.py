calendars = {
    'tech-squares':'tech.squares@gmail.com',
    'ccs':'c2gh23lh5oqdnag0btki55mvsc@group.calendar.google.com',
    'c3s':'ki4826jadjsqtralaancbdrjkc@group.calendar.google.com',
}

class Room:
    """Represents a room that might hold a dance

    snip: snippet that might appear in a calendar entry as the location
    cac: corresponding CAC room name
    """
    def __init__(self, snip, cac):
        self.snip = snip
        self.cac = cac

    def __repr__(self):
        return 'Room(%s)' % (self.snip, )

    def __str__(self):
        return self.cac

rooms = [
    Room("Morss",   cac="Walker (Bldg.50) - Morss Hall"),
    Room("Lobdell", cac="Student Center (Bldg. W20) - Lobdell"),
    Room("Sala",    cac="Student Center (Bldg. W20) - Sala De Puerto Rico"),
    Room("407",     cac="Student Center (Bldg. W20) - Room 407"),
    Room("491",     cac="Student Center (Bldg. W20) - Room 491"),
    Room("West Lounge", cac="Student Center (Bldg. W20) - West Lounge"),
    Room("E15",     cac=False), # Media Lab basement

    # Common classrooms
    Room("2-131",   cac=False),
    Room("2-135",   cac=False),
    Room("4-261",   cac=False),
    Room("36-112",  cac=False),
    Room("66-168",  cac=False),
]
