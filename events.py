#!/usr/bin/env python3

"""
Script to scrape upcoming event schedule from CAC-EMS and display it
"""

import argparse
import collections
import datetime
import email.message
import json
import os
import re
import smtplib
import sys
import time

from googleapiclient.discovery import build
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import Options
from selenium.webdriver.common.keys import Keys
from xvfbwrapper import Xvfb

import data

def parse_args():
    """Set up argument parsing"""
    parser = argparse.ArgumentParser()
    parser.add_argument('--cac', default=False, action='store_true',
                        help='run CAC scraper')
    parser.add_argument('--gcal', default=False, action='store_true',
                        help='fetch Google Calendar entries')
    parser.add_argument('--upcoming', default=False, action='store_true',
                        help='format upcoming events')
    parser.add_argument('--days', default=28, type=int,
                        help='number of days of upcoming events to show')
    cac_help = 'JSON file for CAC schedule (written by --cac, read by --upcoming)'
    parser.add_argument('--cac-json', default='cac.json', help=cac_help)
    gcal_help = 'JSON file for Google Calendar (written by --gcal, read by --upcoming)'
    parser.add_argument('--gcal-json', default='gcal.json', help=gcal_help)
    upcoming_help = 'Text file to write upcoming events to'
    parser.add_argument('--upcoming-file', default='upcoming.txt', help=upcoming_help)

    emails = parser.add_argument_group('Sending emails')
    emails.add_argument('--email', default=False, action='store_true',
                        help='send upcoming events email')
    emails.add_argument('--email-to', default='testing@mit.edu')
    emails.add_argument('--email-bcc', default='squares-db-outgoing@mit.edu')
    emails.add_argument('--email-from', default='squares-db-auto@mit.edu')
    emails.add_argument('--email-server', default='smtp.dehnerts.com')
    emails.add_argument('--email-port', default=587, type=int)
    emails.add_argument('--email-user', default='cacscraper')

    args = parser.parse_args()
    if not (args.cac or args.gcal or args.upcoming or args.email):
        parser.error('must pass at least one mode argument')
    return args


def fetch_schedule_json(out_file, password):
    """Download event schedule from CAC-EMS website, as JSON"""
    chrome_options = Options()
    #chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.binary_location = '/usr/bin/chromium-browser'
    driver = webdriver.Chrome(chrome_options=chrome_options)

    driver.get("https://cac-ems2.mit.edu/EmsWebApp/")
    assert "Home" in driver.title

    elem = driver.find_element_by_xpath("//a[@href='#my-home']")
    elem.send_keys(Keys.ENTER)
    time.sleep(1) # give time to switch tabs

    elem = driver.find_element_by_xpath("//input[@id='userID_input']")
    elem.send_keys("squares@mit.edu")

    elem = driver.find_element_by_name("password_input")
    elem.send_keys(password, Keys.ENTER)

    driver.get("https://cac-ems2.mit.edu/EmsWebApp/BrowseReservations.aspx")
    assert "My Events" in driver.title

    html = driver.page_source
    match = re.search(r"ko.mapping.fromJS\((.*)\);", html)

    cac_json = json.loads(match.group(1))
    with open(out_file, 'w') as cac_fp:
        json.dump(cac_json, fp=cac_fp, indent=2)

    driver.close()


NO_PASSWORD_HELP = """
No %(name)s provided.  Please re-run with the %(var)s env var
populated appropriately.  For example, try running:
read -p '%(name)s: ' -s %(var)s; export %(var)s; echo
and to enter it without echoing.
""".lstrip()

def fetch_secret(name, var):
    """Retrieve a secret from an env var or report an error"""
    try:
        return os.environ[var]
    except KeyError:
        msg = NO_PASSWORD_HELP % dict(name=name, var=var)
        print(msg, file=sys.stderr)
        raise


def do_cac(out_file):
    """Wrapper for fetch_schedule_json, setting up password and X server"""
    password = fetch_secret(name='CAC-EMS password', var='CAC_PASSWORD').strip('_')

    with Xvfb() as xvfb:
        # We're just using xvfb as a context manager -- no need to actually
        # access the variable
        xvfb # pylint:disable=pointless-statement
        fetch_schedule_json(out_file, password=password)


def do_gcal(out_file):
    """Fetch events from Google Calendar"""
    api_key = fetch_secret(name='Google API key', var='GOOGLE_KEY')
    service = build('calendar', 'v3', developerKey=api_key)
    now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
    results = {}
    for cal_name, cal_id in data.calendars.items():
        # pylint:disable=no-member
        events_result = service.events().list(calendarId=cal_id, timeMin=now,
                                              maxResults=20, singleEvents=True,
                                              orderBy='startTime').execute()
        results[cal_name] = events_result

    with open(out_file, 'w') as out_fp:
        json.dump(results, fp=out_fp, indent=2)


def parse_iso8601(string):
    """Parse an ISO8601 string into a datetime object"""
    return datetime.datetime.strptime(string, '%Y-%m-%dT%H:%M:%S')


def parse_iso8601_tz(string):
    """Parse an ISO8601 string with timezone into a datetime object"""
    # Apparently Google thinks the timezone should have a colon, but Python
    # disagrees, so remove the rightmost colon
    pre, _colon, post = string.rpartition(':')
    return datetime.datetime.strptime(pre+post, '%Y-%m-%dT%H:%M:%S%z')


def date_fromisoformat(string):
    """Parse an ISO8601 date string (~Py3.7's date.fromisoformat)"""
    return datetime.datetime.strptime(string, '%Y-%m-%d').date()


def gcal_by_date(gcal_file):
    """Load GCal file and group events by date"""
    with open(gcal_file, 'r') as gcal_fp:
        gcal = json.load(gcal_fp)

    ret = collections.defaultdict(list)
    for _cal_name, cal_data in gcal.items():
        items = cal_data['items']
        for item in items:
            start = item['start']
            if 'dateTime' in start:
                date = parse_iso8601_tz(start['dateTime']).date()
            elif 'date' in start:
                date = date_fromisoformat(start['date'])
            else:
                raise ValueError("Unknown date for calendar entry: %s" % (item, ))
            ret[date].append(item)
    return ret


def save_rooms(event, room_matcher, rooms_using, unknown_rooms):
    """Update the set of rooms that events are using

    event: event dictionary, likely with a "location" field
    room_matcher: lambda((Room,str)->bool), indicating whether this event
      occurs in a given Room
    rooms_using: (out param) set of rooms that events are using
    unknown_rooms: (out param) set of room strings that weren't recognized

    Returns: text to append to event entry (typically blank)
    """

    for room in data.rooms:
        if room_matcher(room, event.get('location', '???')):
            rooms_using.add(room)
            break
    else:
        unknown_rooms.add(event.get('location', '???'))
        return "Unknown room: " + event.get('location', '???') + "\n"
    return ""


def format_gcal_time_range(event):
    """Format the start to end time of an event

    Supports both normal, couple hour events and all-day events"""
    start = event['start']
    end = event['end']
    if 'dateTime' in start and 'dateTime' in end:
        start_time = parse_iso8601_tz(start['dateTime'])
        end_time = parse_iso8601_tz(end['dateTime'])
        return "%s--%s" % (start_time.time(), end_time.time())
    if 'date' in start and 'date' in end:
        return "(all-day)"
    return "(partially all-day????)"


def format_day(date, cac_events, gcal_events, cac_unknown, gcal_unknown):
    """Format a day of events"""
    text = ("%s -- %d CAC events, %d GCal events:\n\n" %
            (date.strftime('%A, %b %d'), len(cac_events), len(gcal_events)))
    cac_rooms = set()
    for event in cac_events:
        text += "CAC: %(name)s\n%(location)s\n" % event
        start_time = parse_iso8601(event['start'])
        end_time = parse_iso8601(event['end'])
        text += "%s--%s\n" % (start_time.time(), end_time.time())
        text += save_rooms(event, lambda room, loc: room.cac == loc, cac_rooms, cac_unknown)
        text += "\n"
    gcal_rooms = set()
    for event in gcal_events:
        text += "GCal: " + event['summary'] + "\n"
        text += "Location: " + event.get('location', '???') + "\n"
        text += format_gcal_time_range(event) + "\n"
        text += save_rooms(event, lambda room, loc: room.snip in loc, gcal_rooms, gcal_unknown)
        text += "\n"
    gcal_cac_rooms = {room for room in gcal_rooms if room.cac}
    if gcal_cac_rooms:
        text += "CAC rooms in GCal: %s\n" % (gcal_cac_rooms, )
    summary = ""
    if cac_rooms != gcal_cac_rooms:
        params = dict(day=date.strftime('%a, %b %d'), cac=cac_rooms, gcal=gcal_cac_rooms)
        summary = "  * %(day)s: CAC %(cac)s vs GCal CAC %(gcal)s\n" % params
        text += "*Rooms differ*: CAC %(cac)s vs GCal CAC %(gcal)s\n" % params
    while not text.endswith('\n\n\n'):
        # We want two completely blank lines between days for clarity
        text += "\n"
    return summary, text


def format_upcoming(gcal, bookings, days):
    """Format upcoming events"""
    # We do have a lot of locals, but they seem contained enough to be fine
    #pylint:disable=too-many-locals
    start_date = datetime.date.today()
    end_date = start_date + datetime.timedelta(days=days)

    date = start_date
    diff_days = ""
    text = ""
    diff_count = 0
    cac_unknown = set()
    gcal_unknown = set()
    while date < end_date:
        day = date.strftime('%Y-%m-%d')
        cac_events = bookings.get(day, {}).get('eventList', [])
        # filter out Special Cancellation events
        cac_events = [d for d in cac_events if d['status'] != 'Special Cancellation']
        gcal_events = gcal.get(date, [])
        if cac_events or gcal_events:
            res = format_day(date, cac_events, gcal_events, cac_unknown,
                             gcal_unknown)
            diff_days += res[0]
            text += res[1]
            diff_count += bool(res[0])
        date = date + datetime.timedelta(days=1)

    diff_msg = "%d days with room differences found\n" % (diff_count, )
    prefix = ""
    if diff_count:
        prefix += "Warning: " + diff_msg
        prefix += diff_days
    if cac_unknown:
        prefix += "Warning: Unknown CAC rooms: %s\n" % (cac_unknown, )
    if gcal_unknown and gcal_unknown != set(['???']):
        prefix += "Warning: Unknown GCal rooms: %s\n" % (gcal_unknown, )
    if prefix:
        prefix += "\n"
    return prefix + text


def do_upcoming(cac_file, gcal_file, msg_file, days):
    """Format upcoming events for easy reading"""
    gcal = gcal_by_date(gcal_file)
    with open(cac_file, 'r') as cac_fp:
        cac = json.load(cac_fp)
    bookings = cac['monthlyBookings']
    text = format_upcoming(gcal, bookings, days)
    with open(msg_file, 'w') as msg_fp:
        msg_fp.write(text)


def do_email(args):
    """Send emails about upcoming events"""
    password = fetch_secret(name='SMTP password', var='SMTP_PASSWORD').strip('_')
    with open(args.upcoming_file, 'r') as upcoming_fp:
        upcoming = upcoming_fp.read()
    msg = email.message.EmailMessage()
    msg['To'] = args.email_to
    msg['From'] = args.email_from
    msg['Subject'] = 'Upcoming events'
    msg.set_content(upcoming)
    with smtplib.SMTP(args.email_server, args.email_port) as smtp:
        smtp.starttls()
        smtp.login(user=args.email_user, password=password)
        rcpts = [args.email_to, args.email_bcc]
        smtp.send_message(msg, to_addrs=rcpts)


def main():
    """Main function"""
    args = parse_args()
    if args.cac:
        do_cac(args.cac_json)
    if args.gcal:
        do_gcal(args.gcal_json)
    if args.upcoming:
        do_upcoming(args.cac_json, args.gcal_json, args.upcoming_file, args.days)
    if args.email:
        do_email(args)


if __name__ == '__main__':
    main()
