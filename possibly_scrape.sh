#!/bin/bash -e

cp_if_needed () {
    if [[ ! ( -a "$1.json" ) ]]; then
        echo "No $1.json, so copying in sample"
        cp "$1.sample.json" "$1.json"
    else
        echo "Using old $1.json"
    fi
}

if [[ -n "$CAC_PASSWORD" ]]; then
    # We have the CAC password, so we *can* scrape
    echo "Have CAC password, so scraping"
    time ./events.py --cac
else
    cp_if_needed cac
fi

if [[ -n "$GOOGLE_KEY" ]]; then
    echo "Have Google API key, so scraping"
    time ./events.py --gcal
else
    cp_if_needed gcal
fi
