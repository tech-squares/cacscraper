# Overview
This script compares CAC room reservations with Google Calendar entries for upcoming Tech Squares events and ensures that they match. This helps us cancel unwanted CAC reservations in a timely manner and keep locations on the Google Calendars up-to-date.

# Operating system
We've only tested this on Linux operating systems. We have not been successful in getting this to work with Windows Subsystem for Linux (WSL).

# Required packages
* Install required python packages from requirements.txt.
* Install Xvfb and chromium-chromedriver using your OS's package manager. For example, on Ubuntu, run 'apt install xvfb chromium-chromedriver'.

This script is generally run by CI on GitLab, so [`.gitlab-ci.yml`](.gitlab-ci.yml) should reliably provide a precise list of installation steps (at least on one OS).

# Files in this project
* `events.py` contains functions for extracting information from the CAC system, extracting information from the google Calendar, comparing the CAC reservations with the Google Calendar entries, and sending email with the comparison results.
* `data.py` contains the urls of the relevant Google Calendars and the information needed to match Google Calendar rooms to CAC rooms.
* `.gitlab-ci.yml` tells GitLab how to run the script.
* `possibly_scrape.sh` fetches the information from the CAC system and Google Calendar when the password and/or API key are provided, or copies over the information in `cac.sample.json` and/or `gcal.sample.json` for testing purposes if not.
* `cac.sample.json` contains sample output for running the script to fetch information from the CAC room reservations page.
* `gcal.sample.json` contains sample output for running the script to fetch information from the Google Calendars.

# Running the code
* In general, GitLab runs the script periodically, following `.gitlab-ci.yml`.
* To run the script manually on your own computer, run `python3 events.py` with various arguments.
  * `--cac` fetches the room reservation information from the CAC website.
  * `--gcal` fetches scheduled event information from the Google Calendars.
  * `--upcoming` compares the CAC and Google Calendar data.
  * `--email` sends email summarizing the results of the comparison.
